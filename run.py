from flask import Flask


app = Flask(__name__)
version = 1.3

@app.route('/', methods=['GET'])
def losowaliczba():
	import random
	a = 1
	b = 1000
	liczba = random.randint(a, b)
	file = open("/python-docker/archive/output.txt", "a")
	file.write(str(liczba)+"\n")
	return f"{liczba}"
	file.close()

@app.route('/author/<name>')
def przywitaj(name):
	return f"Wersja Twojej aplikacji to: {version} <br> Dzień dobry, {name}"


print(version)
if __name__ == "__main__":
	app.run(debug=True)
